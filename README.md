# AUG-Environment #
This a vagrant environment that will create an environment to run the
aug site. If you have the met environment, you can have the website up
and running by cloning this repo, running `vagrant up`, ssh-ing into
the box and running `gulp serve-dev`. You now have a postgres install
with the correct users, you have a java server running in tomcat
handling auth and the angular spa wrapped in angular and running on an
express 4 server. Congratulations, the world is now your oyster.

## Dependencies ##
- Virtual Box
- Vagrant

We are creating a virtual machine and therefore we need a
hypervisor. Unless you have an preference, and the accompanying
expertise to override the default, you will need to install
VirtualBox. Then, install Vagrant, a virtual machine provider to
manage everything. Vagrant allows for provisioning to happen by giving
a script rather than a 400 mb image. You will create the machine from
scratch from these instructions.

## Instructions ##
Clone this repo, and then type `vagrant up`. You are now provisioning
your machine. This will take a while the first time, as you will be
downloading Linux, postgres, java jdk, tomcat, node, and the two repos
that we are interested in. However, Vagrant is smart enough to keep
the linux image cached for future use. So if you want to throw away
the virtual machine, you can easily reprovision it in the future.

To get the website running, type the command `vagrant ssh`. You have
now ssh-ed into the machine and are running in a known and setup
environment.

Pending some accepted pull changes to aug-spa, at this point you can
`cd aug-spa` and run gulp commands here.

- `gulp serve-dev` will run a development server
- `gulp build` will concatenate, minify, vet, and bundle the website
  for production
- `gulp serve-specs` will serve a webpage that will display all unit
  tests and their results. Code changes will trigger a page refresh

## First Time Instructions ##
Since the database is freshly installed, you must run any create
scripts that are required. At this point, this is just the user_auth
create.sql script. To do so, navigate to
`/vagrant/aug-auth/src/main/resources` and we will need to run the
create.sql script there by running the following:
`PGUSER=auth PGPASSWORD=auth psql -h localhost -p 5432 auth -f
create.sql`

This will create the table, but will obviously error if the table
already exists

## What the provision scripts do ##
The provision scripts will setup postgres, tomcat, maven,
node, git, mercurial and the jdk.

There are several setup files that are copied to the correct place to
enable maven building and CORS between the javascript express server
and the tomcat java auth backend. Finally, the two projects aug-spa
and aug-auth are cloned if they are not already present.

# TODO/Improvements #
- Have inital box setup run all create scripts automatically
