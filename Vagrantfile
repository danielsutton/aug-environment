# -*- mode: ruby -*-
# vi: set ft=ruby :

$script = <<SCRIPT
echo I am provisioning...
date > /etc/vagrant_provisioned_at

apt-get install -y git
apt-get install -y mercurial
apt-get install -y maven
apt-get install -y tomcat7
apt-get install -y tomcat7-admin
apt-get install -y openjdk-7-jdk
apt-get install -y nodejs
apt-get intsall -y npm
# softlink nodejs to node
ln -s /usr/bin/nodejs /usr/bin/node
npm install -g gulp

# setup tomcat settings for maven
cp /vagrant/tomcat-users.xml /etc/tomcat7/tomcat-users.xml
mkdir -p ~/.m2
cp /vagrant/settings.xml ~/.m2/settings.xml
cp /vagrant/web.xml /etc/tomcat7/web.xml

# check if projects are cloned already

install_project() {
  if [ ! -d $1 ]; then
    echo "installing project $1"
    hg clone https://bitbucket.org/cparich/$1
  else
    echo "$1 project already installed"
  fi
}

install_project "aug-auth"
install_project "aug-spa"

SCRIPT

Vagrant.configure("2") do |config|
  config.vm.provision "shell", inline: $script

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "2048", "--cpus", "2"]
  end
end

Vagrant::Config.run do |config|

  config.vm.box = "ubuntu/trusty64"
  config.vm.box_url = "https://atlas.hashicorp.com/ubuntu/boxes/trusty64"
  config.vm.host_name = "postgresql"

  config.vm.share_folder "bootstrap", "/mnt/bootstrap", ".", :create => true
  config.vm.provision :shell, :path => "Vagrant-setup/bootstrap.sh"

  # PostgreSQL Server port forwarding
  config.vm.forward_port 5432, 15432

  #tomcat port
  config.vm.forward_port 8080, 8080
  config.vm.forward_port 3000, 3000

end
